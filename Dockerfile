FROM python:3.7-alpine

# install dependencies
RUN apk update && \
#    apk add --virtual build-deps gcc python-dev musl-dev libffi-dev libressl-dev && \
    apk add gcc python3-dev musl-dev libffi-dev libressl libressl-dev && \
    apk add postgresql-dev && \
    apk add --no-cache bash git openssh

# copy requirements.txt
COPY requirements.txt /usr/src/app/requirements.txt

# set working directory
WORKDIR /usr/src/app

# install requirements
RUN pip install --upgrade pip && pip install -r requirements.txt